# -*- encoding: utf-8 -*-
import cv2
import mediapipe as mp
import numpy as np

holistic = mp.solutions.holistic
mp_holistic = holistic.Holistic(min_detection_confidence=0.7, min_tracking_confidence=0.5)


def showPose(img, imgRGB):
    holistic_result = mp_holistic.process(imgRGB)
    # draw.draw_landmarks(img, holistic_result.face_landmarks, holistic.FACEMESH_CONTOURS, styleDot, styleLine)
    h, w, c = img.shape
    if holistic_result.pose_landmarks:
        ids1 = []
        ids2 = []
        ids3 = []
        ids4 = [[], [], [], []]
        ids5 = [[], [], [], [], [], []]
        for id, lm in enumerate(holistic_result.pose_landmarks.landmark):
            cx, cy = int(lm.x * w), int(lm.y * h)
            if id == 0:
                ids1.append([cx, cy])
                ids2.append([cx, cy])
            elif id == 4 or id == 5 or id == 6 or id == 8:
                ids1.append([cx, cy])
            elif id == 1 or id == 2 or id == 3 or id == 7:
                ids2.append([cx, cy])
            elif id == 10 or id == 9:
                ids3.append([cx, cy])
            elif id == 11:
                ids4[1] = [cx, cy]
            elif id == 12:
                ids4[2] = [cx, cy]
            elif id == 13:
                ids4[0] = [cx, cy]
            elif id == 14:
                ids4[3] = [cx, cy]
            elif id == 27:
                ids5[0] = [cx, cy]
            elif id == 25:
                ids5[1] = [cx, cy]
            elif id == 23:
                ids5[2] = [cx, cy]
            elif id == 24:
                ids5[3] = [cx, cy]
            elif id == 26:
                ids5[4] = [cx, cy]
            elif id == 28:
                ids5[5] = [cx, cy]

        cv2.polylines(img, pts=[np.array(ids1, np.int32)], isClosed=False, color=(155, 155, 255), thickness=2)
        cv2.polylines(img, pts=[np.array(ids2, np.int32)], isClosed=False, color=(155, 255, 255), thickness=2)
        cv2.polylines(img, pts=[np.array(ids3, np.int32)], isClosed=False, color=(155, 255, 155), thickness=3)
        cv2.polylines(img, pts=[np.array(ids4, np.int32)], isClosed=False, color=(155, 15, 155), thickness=3)
        cv2.polylines(img, pts=[np.array(ids5, np.int32)], isClosed=False, color=(155, 15, 155), thickness=3)

        cv2.circle(img, (ids1[0][0], ids1[0][1]), 12, (255, 255, 255), cv2.FILLED)
        for i, _ in enumerate(ids1):
            if i > 0:
                cv2.circle(img, (ids1[i][0], ids1[i][1]), 8, (0, 101, 121), cv2.FILLED)
        for i, _ in enumerate(ids2):
            if i > 0:
                cv2.circle(img, (ids2[i][0], ids2[i][1]), 8, (121, 101, 0), cv2.FILLED)
        for i, _ in enumerate(ids3):
            cv2.circle(img, (ids3[i][0], ids3[i][1]), 12, (45, 91, 253), cv2.FILLED)

        for i, _ in enumerate(ids4):
            cv2.circle(img, (ids4[i][0], ids4[i][1]), 12, (158, 111, 254), cv2.FILLED)

        for i, _ in enumerate(ids5):
            cv2.circle(img, (ids5[i][0], ids5[i][1]), 12, (158, 111, 254), cv2.FILLED)

    return img
